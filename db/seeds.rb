# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

countries = Country.create([{code: 'ET', name: 'Ethiopia'}, {code: 'CO', name: 'Colombia'}, {code: 'BI', name: 'Burundi'}])

# Ethiopian Regions
tmp_country = Country.where(code: 'ET').first
['Sidamo', 'Ghimbi', 'Harrar'].each {|region_name|
    Region.create({name: region_name, country_id: tmp_country.id})
}

# Burundi Regions
tmp_country = Country.where(code: 'BI').first
['Buyenzi', 'Kirundo', 'Muyinga', 'Gitega', 'Bubanza'].each {|region_name|
    Region.create({name: region_name, country_id: tmp_country.id})
}

# Colombia Regions
# ...There are a lot more of these than what's included here
tmp_country = Country.where(code: 'CO').first
['Nariño', 'Norte de Santander', 'Antioquia', 'Valle del Cauca'].each {|region_name|
    Region.create({name: region_name, country_id: tmp_country.id})
}


